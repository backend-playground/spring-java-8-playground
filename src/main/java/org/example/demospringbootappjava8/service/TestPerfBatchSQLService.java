package org.example.demospringbootappjava8.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.*;
import java.util.Arrays;

@Component
@Slf4j
@AllArgsConstructor
public class TestPerfBatchSQLService {

    JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void start() {
        long startTime = System.currentTimeMillis();

        String url = "jdbc:db2://localhost:50000/testdb";
        String user = "db2inst1";
        String password = "P@ssw0rd";

        Connection connection1 = null;
        Connection connection2 = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;

        try {

            String sql1 = "INSERT INTO CUSTOMER ( NAME) VALUES (?)";
            String sql2 = "INSERT INTO CUSTOMER2 ( NAME) VALUES (?)";

            int length = 230000;
            int[] batchResult1;
            int[] batchResult2;
            int batchResult1Length = 0;
            int batchResult2Length = 0;
            int fail1Amt = 0;
            int fail2Amt = 0;


            connection1 = DriverManager.getConnection(url, user, password);
            connection1.setAutoCommit(false);
            connection2 = DriverManager.getConnection(url, user, password);
            connection2.setAutoCommit(false);

            preparedStatement1 = connection1.prepareStatement(sql1);
            preparedStatement2 = connection2.prepareStatement(sql2);
            for (int i = 0; i < length; i++) {

                preparedStatement1.setString(1, "Customer " + i); // Name
                preparedStatement1.addBatch();



                if (i % 1000 == 0 || i == length-1){
                    System.out.println("round 1 : " + i);
                    batchResult1 = preparedStatement1.executeBatch();
                    int fail1 = Arrays.stream(batchResult1)
                            .filter(x -> x != 1)
                            .sum();
                    batchResult1Length += batchResult1.length;
                    fail1Amt += fail1;
                    preparedStatement1.clearBatch();

                }

            }

            connection1.commit();



            for (int i = 0; i < length; i++) {

                preparedStatement2.setString(1, "Customer " + i); // Name
                preparedStatement2.addBatch();


                if (i % 1000 == 0 || i == length-1){
                    System.out.println("round 2 : " + i);



                    batchResult2 = preparedStatement2.executeBatch();
                    int fail2 = Arrays.stream(batchResult2)
                            .filter(x -> x != 1)
                            .sum();
                    batchResult2Length += batchResult2.length;
                    fail2Amt += fail2;
                    preparedStatement2.clearBatch();
                }

            }


            connection2.commit();




//            batchResult1 = preparedStatement1.executeBatch();
//            batchResult2 = preparedStatement2.executeBatch();
            log.info("INSERTING TXN LOG: Execution all " + batchResult1Length + " transactions and have failed " + fail1Amt + " transactions");
            log.info("UPDATING TXN AMOUNT: Execution all " + batchResult2Length + " transactions and have failed " + fail2Amt + " transactions");
            System.out.println("Batch execution successful.");

            if (fail1Amt == 0 && fail2Amt == 0) {
                connection1.commit();
                log.info("execution all transactions successful");
            } else {
                log.info("rollback all transactions because some transaction is failed");
            }
        } catch (BatchUpdateException e) {
            SQLException bue = e.getNextException();
            while (bue != null) {
                System.err.println("Error executing batch: " + bue.getErrorCode() + " => " + bue.getMessage());
                bue = bue.getNextException();
            }
            try {
                if (connection1 != null)
                    connection1.rollback();
                if (connection2 != null)
                    connection2.rollback();
            } catch (SQLException ex) {
                log.error("err SQLException ", e);
            }

        } catch (Exception e) {
            try {
                if (connection1 != null) {
                    connection1.rollback();
                }
                if (connection2 != null)
                    connection2.rollback();
            } catch (SQLException ex) {
                log.error("err SQLException ", e);
            }
            log.error("err try ", e);
        } finally {
            log.info("finally");
            try {
                if (preparedStatement1 != null) {
                    preparedStatement1.close();
                }

                if (preparedStatement2 != null) {
                    preparedStatement2.close();
                }
                if (connection1 != null) {
                    connection1.close();
                }
                if (connection2 != null) {
                    connection2.close();
                }
            } catch (SQLException e) {
                log.error("err finally ", e);
            }
        }
        long endTime = System.currentTimeMillis();
        long executionTime = endTime - startTime;
        log.info("SettleTrigger: Execution time: " + executionTime / 1000.0 + " seconds");
    }
}
