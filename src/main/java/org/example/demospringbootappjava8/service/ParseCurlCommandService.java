package org.example.demospringbootappjava8.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ParseCurlCommandService {

    public Map<String, String> parseCurlCommand(String curlCommand) {
        Map<String, String> components = new HashMap<>();

        // Extracting HTTP Method
        Pattern methodPattern = Pattern.compile("(?i)-X\\s+([A-Z]+)");
        Matcher methodMatcher = methodPattern.matcher(curlCommand);
        if (methodMatcher.find()) {
            components.put("method", methodMatcher.group(1));
        } else {
            components.put("method", "GET"); // Default to GET if method not specified
        }

        // Extracting URL
        Pattern urlPattern = Pattern.compile("(?i)\\s([a-z]+://[^\\s']+)");
        Matcher urlMatcher = urlPattern.matcher(curlCommand);
        if (urlMatcher.find()) {
            components.put("url", urlMatcher.group(1));
        }

        // Extracting Headers
        Pattern headerPattern = Pattern.compile("(?i)-H\\s+'([^']+: [^']+)+'");
        Matcher headerMatcher = headerPattern.matcher(curlCommand);
        StringBuilder headers = new StringBuilder();
        while (headerMatcher.find()) {
            headers.append(headerMatcher.group(1)).append("\n");
        }
        components.put("headers", headers.toString().trim());

        // Extracting Body
        Pattern dataPattern = Pattern.compile("(?i)-d\\s+'([^']*)'");
        Matcher dataMatcher = dataPattern.matcher(curlCommand);
        if (dataMatcher.find()) {
            components.put("body", dataMatcher.group(1));
        }

        return components;
    }
}
