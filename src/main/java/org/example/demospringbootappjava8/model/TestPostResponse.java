package org.example.demospringbootappjava8.model;

import lombok.Data;

@Data
public class TestPostResponse {
    private String a;
    private int b;
}
