package org.example.demospringbootappjava8.model;

import lombok.Data;

@Data
public class TestPostRequest {
    private String x;
    private int y;
}
