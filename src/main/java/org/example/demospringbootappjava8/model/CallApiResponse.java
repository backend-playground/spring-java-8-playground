package org.example.demospringbootappjava8.model;

import lombok.Data;

import java.util.Map;

@Data
public class CallApiResponse {
    private String code;
    private String response;
    private Map<String,String> headers;
}
