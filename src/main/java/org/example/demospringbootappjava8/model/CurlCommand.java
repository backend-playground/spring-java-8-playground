package org.example.demospringbootappjava8.model;

import lombok.Data;

@Data
public class CurlCommand {
    private String command;
}
