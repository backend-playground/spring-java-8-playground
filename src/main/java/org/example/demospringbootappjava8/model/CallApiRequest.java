package org.example.demospringbootappjava8.model;

import lombok.Data;

import java.util.Map;

@Data
public class CallApiRequest {
    private String url;
    private String method;
    private String body;
    private Map<String,String> headers;
}
