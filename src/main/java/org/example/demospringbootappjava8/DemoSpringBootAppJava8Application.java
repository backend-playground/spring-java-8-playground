package org.example.demospringbootappjava8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBootAppJava8Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringBootAppJava8Application.class, args);
    }

}
