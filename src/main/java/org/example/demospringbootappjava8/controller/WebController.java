package org.example.demospringbootappjava8.controller;

import org.example.demospringbootappjava8.model.CurlCommand;
import org.example.demospringbootappjava8.model.TestPostRequest;
import org.example.demospringbootappjava8.model.TestPostResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.example.demospringbootappjava8.service.ParseCurlCommandService;

import java.util.Map;

@RestController
public class WebController {

    @Autowired
    private ParseCurlCommandService ParseCurlCommandService;

    @GetMapping("/hi")
    public String hi(){
        return "Hi";
    }

    @PostMapping("/curl")
    public Map<String, String> curl(@RequestParam(value = "cmd") String cmd){
        System.out.println(cmd);
        Map<String, String> components = ParseCurlCommandService.parseCurlCommand(cmd);
        System.out.println("HTTP Method: " + components.get("method"));
        System.out.println(" ");
        System.out.println("URL: " + components.get("url"));
        System.out.println(" ");
        System.out.println("Headers: \n" + components.get("headers"));
        System.out.println(" ");
        System.out.println("Body: " + components.get("body"));
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        return components;
    }

    @PostMapping("/test-post")
    public TestPostResponse testPost(@RequestBody TestPostRequest req){
        TestPostResponse res = new TestPostResponse();
        res.setA(req.getX());
        res.setB(req.getY());
        return res;
    }
}
